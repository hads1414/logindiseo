package com.help.animal.animalhelp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;

public class login extends AppCompatActivity implements View.OnClickListener {

    //FirebaseDatabase firebaseDatabase;
    //DatabaseReference databaseReference;
    private FirebaseAuth mAuth;
    private EditText Email, Contraseña;
    private Button Login, Registro;
    private String TAG;
    private FirebaseAuth firebaseAuth;
    private ProgressDialog progressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        // inicia el objeto firebaseAuth
        firebaseAuth = FirebaseAuth.getInstance();
        //referenciamos los views
        Email = (EditText) findViewById(R.id.txtEmail);
        Contraseña = (EditText) findViewById(R.id.txtPassword);

        Login = (Button) findViewById(R.id.btnIngreso);
        Registro = (Button) findViewById(R.id.btnRegistro);

        progressDialog = new ProgressDialog(this);

        Login.setOnClickListener(this);
        Registro.setOnClickListener(this);
    }


    private void registrarUsuario() {
        //se obtiene el email de la caja de texto
        String email = Email.getText().toString().trim();
        String password = Contraseña.getText().toString().trim();
        //Verificamos que la caja de texto no este vacia
        if (TextUtils.isEmpty(email)) {
            Toast.makeText(this, "Se debe ingresar un Email", Toast.LENGTH_SHORT).show();
            return;
        }
        if (TextUtils.isEmpty(password)) {
            Toast.makeText(this, "falta ingresar contraseña", Toast.LENGTH_SHORT).show();
            return;
        }

        progressDialog.setMessage("Realizando registro en linea");
        progressDialog.show();

        //creacion de un nuevo usuario
        firebaseAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        //checking if success
                        if (task.isSuccessful()) {

                            Toast.makeText(login.this, "Se ha registrado el usuario con el email: " + Email.getText(), Toast.LENGTH_LONG).show();
                        } else {
                            if (task.getException() instanceof FirebaseAuthUserCollisionException) {//si se presenta una colisión
                                Toast.makeText(login.this, "Ese usuario ya existe ", Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(login.this, "No se pudo registrar el usuario ", Toast.LENGTH_LONG).show();
                            }
                        }
                        progressDialog.dismiss();
                    }
                });
    }

    private void loguearUsuario() {
        //Obtenemos el email y la contraseña desde las cajas de texto
        final String email = Email.getText().toString().trim();
        String password = Contraseña.getText().toString().trim();

        //Verificamos que las cajas de texto no esten vacías
        if (TextUtils.isEmpty(email)) {//(precio.equals(""))
            Toast.makeText(this, "Se debe ingresar un email", Toast.LENGTH_LONG).show();
            return;
        }

        if (TextUtils.isEmpty(password)) {
            Toast.makeText(this, "Falta ingresar la contraseña", Toast.LENGTH_LONG).show();
            return;
        }


        progressDialog.setMessage("Realizando consulta en linea...");
        progressDialog.show();


        //loguear usuario
        firebaseAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        //checking if success
                        if (task.isSuccessful()) {
                            int pos = email.indexOf("@");
                            String user = email.substring(0, pos);
                            Toast.makeText(login.this, "Bienvenido: " + Email.getText(), Toast.LENGTH_LONG).show();
                            Intent intent = new Intent(getApplication(), navegacion.class);
                            intent.putExtra(navegacion.user, user);
                            startActivity(intent);
                            finish();


                        } else {
                            if (task.getException() instanceof FirebaseAuthUserCollisionException) {//si se presenta una colisión
                                Toast.makeText(login.this, "Ese usuario ya existe ", Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(login.this, "No se pudo ingresar con el usuario ", Toast.LENGTH_LONG).show();
                            }
                        }
                        progressDialog.dismiss();
                    }
                });

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnRegistro:
                //Invocamos al método:
                registrarUsuario();
                break;
            case R.id.btnIngreso:
                loguearUsuario();
                break;
        }
    }
}
